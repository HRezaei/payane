﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
namespace Payane
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void OpenFile(String file)
        {
            if (file.EndsWith(".exe"))
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo(file);
                startInfo.UseShellExecute = false;
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.StandardOutputEncoding = Encoding.UTF8;
                process.OutputDataReceived += new DataReceivedEventHandler(ProcessOutput);
                
                startInfo.RedirectStandardInput = true;
                
                //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                //startInfo.Arguments = /*"D:\\masm32\bin\\ml /c /Zd /coff " +*/ "\"" + FileName + ".asm\" \"" + FileName + ".obj\"" /*+ ".asm"*/;
                process.StartInfo = startInfo;
                process.EnableRaisingEvents = true;
                process.Start();

                StreamWriter inputWriter = process.StandardInput;

                process.BeginOutputReadLine();

                process.ErrorDataReceived += new DataReceivedEventHandler(ProcessError);
                /*
                StreamReader outputReader = process.StandardOutput;
                StreamReader errorReader = process.StandardError;
                inputWriter.AutoFlush = true;
                inputWriter.WriteLine(25);
                */richTextBox1.Text += "اجرای فایل داده شده در مسیر :\n" + file + " :\n";
                
                process.WaitForExit();

            }
            else
            {
                richTextBox1.Text += File.ReadAllText(file);
            }
        }

        public void ProcessOutput(object sender, DataReceivedEventArgs e)
        {
            //string output = outputReader.ReadLine();
            if(e.Data!=null)
            MessageBox.Show( e.Data.ToString());
        }

private void ProcessError(Object sender, DataReceivedEventArgs e)
        {
            //string output = outputReader.ReadLine();
            richTextBox1.Text += e.Data.ToString();
        }

        public void Process(String Command)
        {
            if (File.Exists(Command))
            {
                OpenFile(Command);
            }
        }

        private void richTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                Process(richTextBox2.Text.Substring(2, richTextBox2.Text.Length - 3));
                richTextBox2.Text = "->";
                richTextBox2.Focus();
                richTextBox2.SelectionStart = richTextBox2.Text.Length;
            }
        }

        private void richTextBox1_ContentsResized(object sender, ContentsResizedEventArgs e)
        {
            if (e.NewRectangle.Height > richTextBox1.Height)
            {
                richTextBox1.Height = e.NewRectangle.Height;
                //splitContainer1.Height= e.NewRectangle.Height + 20;
                //this.Height = e.NewRectangle.Height + 20;
            }
        }
        /*
        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {
            if (richTextBox2.Text.Length < 6) richTextBox2.Text = "      " + richTextBox2.Text;
            if (richTextBox2.Text.Substring(0, 6) != "      ") richTextBox2.Text="      "+richTextBox2.Text.TrimStart(' ');
        }*/
    }
}
